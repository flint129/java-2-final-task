## Запуск

Чтобы запустить эксперименты и сгенерировать финальный отчет нужно запустить run_experiments.sh.

```shell
bash run_experiments.sh
```

## Интерпретация результатов

Сгенерированный отчет со сравнением архитектур: [report.md](results%2Ffinal_report%2Freport.md)

Получилось, что в среднем асинхронная чуть эффективнее неблокирующей, которая в свою очередь эффективнее
блокирующей. Если предположить, что реализация архитектур не содержит серьезных недостатков, то думаю дело в том, что
в блокирующей архитектуре время исполнение чуть больше чем в остальных, поскольку из-за большого количества
потоков, чуть чаще приходится снимать с исполнения потоки тред пула. Неблокирующая и асинхронная имеют
очень похожие результаты, но асинхронная быстрее, возможно потому что ей нужно еще меньше потоков для
работы и она чуть меньше мешает исполнению задач. То есть в целом думаю, что в проведенных экспериментах достаточно 
сильно влияло именно количество потоков, нужное для работы архитектуры, поскольку и клиенты и сервер запускались на 
одном устройстве из-за чего приходилось чаще менять контекст.  

### Время сортировки на сервере

Зависимость от интервала между запросами выглядит странно. Ожидалось что время будет уменьшаться
с увеличением интервала или оставаться одинаковым, однако больший интервал иногда приводит к
увеличению времени сортировки (причем с похожей зависимостью на всех архитектурах). Возможно
это связано с тем, что при маленьком интервале потоки тред пула реже снимаются с исполнения, поскольку
клиенты и сервер дольше находятся заблокированными на IO. А при большом интервале (около 1000 мс) клиенты достаточно
много спят и чаще есть возможность исполнять задачи без переключений контекста.

![avg_task_execution_time_on_server.png](results%2Ffinal_report%2F0%2Favg_task_execution_time_on_server.png)

С увеличением числа клиентов время сортировки не увеличивается. Также возможно потому что потоки тред пула не часто
снимаются с исполнения из-за большого количества задач. Интересно, что с одним клиентом время исполнения больше,
идей почему так нет (может разве что jit что-то успевает оптимизировать при большом количестве клиентов, а при 1 - нет).

![avg_task_execution_time_on_server.png](results%2Ffinal_report%2F1%2Favg_task_execution_time_on_server.png)

Зависимость от размера задачи выглядит как квадратичная (как и ожидалось).

![avg_task_execution_time_on_server.png](results%2Ffinal_report%2F2%2Favg_task_execution_time_on_server.png)

### Время обработки клиента на сервере

Ожидаемо, с увеличением интервала между запросами время обработки клиента на сервере уменьшается и
остается более менее постоянным. При этом разница между архитектурами становится незначительной.

![avg_client_processing_time_on_server.png](results%2Ffinal_report%2F0%2Favg_client_processing_time_on_server.png)

С увеличением числа клиентов, время обработки одного увеличивается примерно линейно. Увеличение скорее всего
тем сильнее, чем больше потоков использует архитектура для своей работы. Возможно это влияло бы не так сильно,
если бы клиенты не запускались на том же устройстве, что и сервер.

![avg_client_processing_time_on_server.png](results%2Ffinal_report%2F1%2Favg_client_processing_time_on_server.png)

От размера задачи зависимость также квадратичная, как и в прошлом пункте.

![avg_client_processing_time_on_server.png](results%2Ffinal_report%2F2%2Favg_client_processing_time_on_server.png)

### Общее время обработки запроса для клиента

Графики времени обработки запроса для клиента выглядят аналогично графикам с сервера, видимо потому что задержка
сети при запуске на одно устройстве очень небольшая. Однако время исполнения на графиках с клиента чуть меньше
чем на сервере. Думаю это связано с тем, что для сервера учитывали только те запросы, которые производились,
когда были подключены все клиенты, в то время как для клиента учитывалось все время.

![avg_request_processing_time_on_client.png](results%2Ffinal_report%2F0%2Favg_request_processing_time_on_client.png)

![avg_request_processing_time_on_client.png](results%2Ffinal_report%2F1%2Favg_request_processing_time_on_client.png)

![avg_request_processing_time_on_client.png](results%2Ffinal_report%2F2%2Favg_request_processing_time_on_client.png)