plugins {
    java
    application
    id("com.google.protobuf") version "0.9.4"
}

group = "org.itmo"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    implementation("com.google.protobuf:protobuf-java:3.19.6")
    implementation("commons-cli:commons-cli:1.6.0")
    implementation("com.google.code.gson:gson:2.10.1")
    implementation("org.jfree:jfreechart:1.5.0")
}


java {
    sourceSets {
        main {
            java.setSrcDirs(listOf("src/main", "${buildDir}/generated/source/proto/main/java"))
            resources.setSrcDirs(listOf("src/resources"))
            proto.setSrcDirs(listOf("src/main/org/itmo/proto"))
        }
        test {
            java.setSrcDirs(listOf("src/test"))
        }
    }
}

tasks.test {
    useJUnitPlatform()
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:3.19.4"
    }
}

tasks {
    compileJava {
        dependsOn("generateProto")
    }
}

val experimentLauncherJar by tasks.creating(Jar::class) {
    manifest {
        attributes("Main-Class" to "org.itmo.java.experiment.ExperimentLauncher")
    }
    from(sourceSets.main.get().output)
    from({
        configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) }
    })
    exclude("META-INF/versions/9/module-info.class")
    archiveBaseName.set("experimentLauncher")
    archiveVersion.set("")
}

val reportCreator by tasks.creating(Jar::class) {
    manifest {
        attributes("Main-Class" to "org.itmo.java.report.ReportCreator")
    }
    from(sourceSets.main.get().output)
    from({
        configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) }
    })
    exclude("META-INF/versions/9/module-info.class")
    archiveBaseName.set("reportCreator")
    archiveVersion.set("")
}

tasks.named("build").configure {
    dependsOn(experimentLauncherJar, reportCreator)
}
