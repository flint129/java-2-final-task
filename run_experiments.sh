~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s async -x 100 -n 1000:10000:1000 -m 100 -d 10
~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s async -x 100 -n 1000 -m 1:501:50 -d 10
~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s async -x 100 -n 1000 -m 100 -d 10:1010:100

~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s blocking -x 100 -n 1000:10000:1000 -m 100 -d 10
~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s blocking -x 100 -n 1000 -m 1:501:50 -d 10
~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s blocking -x 100 -n 1000 -m 100 -d 10:1010:100

~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s nonblocking -x 100 -n 1000:10000:1000 -m 100 -d 10
~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s nonblocking -x 100 -n 1000 -m 1:501:50 -d 10
~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/experimentLauncher.jar -s nonblocking -x 100 -n 1000 -m 100 -d 10:1010:100

~/.jdks/openjdk-21.0.1/bin/java -jar build/libs/reportCreator.jar
