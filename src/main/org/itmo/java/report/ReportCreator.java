package org.itmo.java.report;

import org.itmo.java.experiment.IntExperimentParameter;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import com.google.gson.Gson;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.google.gson.reflect.TypeToken;

public class ReportCreator {
    private static final Gson gson = new Gson();

    public static void main(String[] args) throws IOException {
        Path resultsDir = Paths.get(".", "results");
        if (!Files.exists(resultsDir)) {
            throw new RuntimeException("result directory doesn't exists");
        }

        List<Path> blockingExperimentsDirs = findLastExperimentsDirectories(resultsDir.resolve("blocking"));
        List<Path> nonblockingExperimentsDirs = findLastExperimentsDirectories(resultsDir.resolve("nonblocking"));
        List<Path> asyncExperimentsDirs = findLastExperimentsDirectories(resultsDir.resolve("async"));

        for (int experimentIndex = 0; experimentIndex < 3; experimentIndex++) {
            Path experimentDir = resultsDir.resolve("final_report").resolve(String.valueOf(experimentIndex));
            if (!Files.exists(experimentDir)) {
                try {
                    Files.createDirectories(experimentDir);
                } catch (IOException e) {
                    throw new RuntimeException("Could not create results directory", e);
                }
            }
            Map<String, Object> experimentParams = getExperimentParameters(blockingExperimentsDirs.get(experimentIndex));
            if (!experimentParams.equals(getExperimentParameters(nonblockingExperimentsDirs.get(experimentIndex))) || !experimentParams.equals(getExperimentParameters(asyncExperimentsDirs.get(experimentIndex)))) {
                throw new RuntimeException("experiments has different parameters");
            }
            Integer requestsNumber = (Integer) experimentParams.get("requests_number");
            IntExperimentParameter taskSize = (IntExperimentParameter) experimentParams.get("task_size");
            IntExperimentParameter clientsNumber = (IntExperimentParameter) experimentParams.get("clients_number");
            IntExperimentParameter requestIntervalMillis = (IntExperimentParameter) experimentParams.get("request_interval_millis");

            createPlots(experimentDir, requestsNumber, taskSize, clientsNumber, requestIntervalMillis, blockingExperimentsDirs.get(experimentIndex), nonblockingExperimentsDirs.get(experimentIndex), asyncExperimentsDirs.get(experimentIndex));
            writeReport(resultsDir.resolve("final_report"));
        }
    }

    static void writeReport(Path finalReportPath) throws IOException {
        String reportContent = """
                ## avg_task_execution_time_on_server
                ![avg_task_execution_time_on_server.png](0%2Favg_task_execution_time_on_server.png)
                ![avg_task_execution_time_on_server.png](1%2Favg_task_execution_time_on_server.png)
                ![avg_task_execution_time_on_server.png](2%2Favg_task_execution_time_on_server.png)
                
                ## avg_client_processing_time_on_server
                ![avg_client_processing_time_on_server.png](0%2Favg_client_processing_time_on_server.png)
                ![avg_client_processing_time_on_server.png](1%2Favg_client_processing_time_on_server.png)
                ![avg_client_processing_time_on_server.png](2%2Favg_client_processing_time_on_server.png)

                ## avg_request_processing_time_on_client
                ![avg_request_processing_time_on_client.png](0%2Favg_request_processing_time_on_client.png)
                ![avg_request_processing_time_on_client.png](1%2Favg_request_processing_time_on_client.png)
                ![avg_request_processing_time_on_client.png](2%2Favg_request_processing_time_on_client.png)

                """;

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(finalReportPath.resolve("report.md").toFile()))) {
            writer.write(reportContent);
        }
    }

    public static void createPlots(Path experimentDir, Integer requestsNumber, IntExperimentParameter taskSize, IntExperimentParameter clientsNumber, IntExperimentParameter requestIntervalMillis, Path blockingResultPath, Path nonblockingResultPath, Path asyncResultPath) throws IOException {
        IntExperimentParameter rangeParameter;
        String description;
        if (requestIntervalMillis.isRange) {
            rangeParameter = requestIntervalMillis;
            description = createDescription(requestsNumber, clientsNumber, taskSize);
        } else if (clientsNumber.isRange) {
            rangeParameter = clientsNumber;
            description = createDescription(requestsNumber, requestIntervalMillis, taskSize);
        } else if (taskSize.isRange) {
            rangeParameter = taskSize;
            description = createDescription(requestsNumber, requestIntervalMillis, clientsNumber);
        } else {
            throw new RuntimeException("Plot creation failed. Range parameter of experiment not found");
        }
        createPlot(experimentDir, "avg_task_execution_time_on_server.json", "Average Task Execution Time On Server", rangeParameter, description, blockingResultPath, nonblockingResultPath, asyncResultPath);
        createPlot(experimentDir, "avg_client_processing_time_on_server.json", "Average Client Processing Time On Server", rangeParameter, description, blockingResultPath, nonblockingResultPath, asyncResultPath);
        createPlot(experimentDir, "avg_request_processing_time_on_client.json", "Average Request Processing Time On Client", rangeParameter, description, blockingResultPath, nonblockingResultPath, asyncResultPath);
    }

    private static void createPlot(Path experimentDir, String metricFileName, String chartTitle, IntExperimentParameter rangeParameter, String description, Path blockingResultPath, Path nonblockingResultPath, Path asyncResultPath) throws IOException {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        Map<String, Path> paths = Map.of("blocking", blockingResultPath, "nonblocking", nonblockingResultPath, "async", asyncResultPath);
        for (String serverArch : paths.keySet()) {
            Path metricFilePath = paths.get(serverArch).resolve(metricFileName);
            if (!Files.exists(metricFilePath)) {
                System.out.println("Metric file not found: " + metricFilePath);
                return;
            }
            Map<?, ?> metricData = gson.fromJson(new FileReader(metricFilePath.toFile()), Map.class);
            List<Double> data = (List<Double>) metricData.get("data");
            int i = 0;
            for (Integer paramValue : rangeParameter) {
                dataset.addValue(data.get(i), serverArch, String.valueOf(paramValue));
                i++;
            }
        }

        JFreeChart chart = ChartFactory.createLineChart(chartTitle, rangeParameter.name, "time, ms", dataset, PlotOrientation.VERTICAL, true, true, false);
        chart.addSubtitle(new TextTitle(description));

        Path plotPath = experimentDir.resolve(metricFileName.replace(".json", ".png"));
        ChartUtils.saveChartAsPNG(plotPath.toFile(), chart, 800, 600);
    }

    public static Map<String, Object> getExperimentParameters(Path experimentDir) {
        Path experimentParamsPath = experimentDir.resolve("experiment.json");
        Gson gson = new Gson();

        try (Reader reader = Files.newBufferedReader(experimentParamsPath)) {
            Map<String, Object> params = gson.fromJson(reader, new TypeToken<Map<String, Object>>() {
            }.getType());

            String serverArch = (String) params.get("server_arch");
            Integer requestsNumber = ((Double) params.get("requests_number")).intValue();
            IntExperimentParameter taskSize = gson.fromJson(gson.toJson(params.get("task_size")), IntExperimentParameter.class);
            IntExperimentParameter clientsNumber = gson.fromJson(gson.toJson(params.get("clients_number")), IntExperimentParameter.class);
            IntExperimentParameter requestIntervalMillis = gson.fromJson(gson.toJson(params.get("request_interval_millis")), IntExperimentParameter.class);

            return Map.of("requests_number", requestsNumber, "task_size", taskSize, "clients_number", clientsNumber, "request_interval_millis", requestIntervalMillis);
        } catch (IOException e) {
            throw new RuntimeException("Failed to restore parameters from experiment.json", e);
        }
    }

    public static List<Path> findLastExperimentsDirectories(Path directory) {
        try (Stream<Path> subdirs = Files.list(directory)) {
            List<Path> subdirectories = subdirs.filter(Files::isDirectory).toList();
            List<Integer> directoryNumbers = subdirectories.stream().map(Path::getFileName).map(n -> Integer.parseInt(n.toString())).toList();
            List<Integer> greatestNumbers = directoryNumbers.stream().sorted(Comparator.reverseOrder()).limit(3).toList();
            List<Path> greatestDirectories = new ArrayList<>();
            for (Integer i : greatestNumbers) {
                greatestDirectories.add(directory.resolve(String.valueOf(i)));
            }
            if (greatestDirectories.size() < 3) {
                throw new RuntimeException("Not enough experiments for final report");
            }
            return greatestDirectories;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String createDescription(Integer requestsNumber, IntExperimentParameter parameter1, IntExperimentParameter parameter2) {
        return String.format("requests number: %d, %s: %d, %s: %d", requestsNumber, parameter1.name, parameter1.startValue, parameter2.name, parameter2.startValue);
    }
}


