package org.itmo.java.experiment;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class IntExperimentParameter implements Iterable<Integer> {
    public int startValue;
    public final boolean isRange;
    public final int endValue;
    public final int step;

    public final String name;

    public IntExperimentParameter(String input, String name) {
        this.name = name;
        if (input.contains(":")) {
            String[] parts = input.split(":");
            if (parts.length != 3) {
                throw new IllegalArgumentException("Invalid format");
            }
            try {
                startValue = Integer.parseInt(parts[0]);
                endValue = Integer.parseInt(parts[1]);
                step = Integer.parseInt(parts[2]);
                isRange = true;
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid format", e);
            }
        } else {
            try {
                startValue = Integer.parseInt(input);
                endValue = startValue;
                step = 1;
                isRange = false;
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid format", e);
            }
        }
    }

    @Override
    public Iterator<Integer> iterator() {
        return new ExperimentParameterIterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntExperimentParameter integers = (IntExperimentParameter) o;
        return startValue == integers.startValue && isRange == integers.isRange && endValue == integers.endValue && step == integers.step && Objects.equals(name, integers.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startValue, isRange, endValue, step, name);
    }

    private class ExperimentParameterIterator implements Iterator<Integer> {
        private int currentValue = startValue;

        @Override
        public boolean hasNext() {
            return currentValue <= endValue;
        }

        @Override
        public Integer next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            int nextValue = currentValue;
            currentValue += step;
            return nextValue;
        }
    }
}