package org.itmo.java.experiment.server.async;

import org.itmo.java.experiment.server.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class AsyncServer implements Server {
    private final int threadPoolSize;
    private final int port;
    private final int clientsNumber;
    private double lastRunAvgTaskExecutionTime;
    private double lastRunAvgClientProcessingTime;

    public AsyncServer(int port, int clientsNumber, int threadPoolSize) {
        this.port = port;
        this.clientsNumber = clientsNumber;
        this.threadPoolSize = threadPoolSize;
    }

    @Override
    public Double getLastRunAvgTaskExecutionTime() {
        return lastRunAvgTaskExecutionTime;
    }

    @Override
    public Double getLastRunAvgClientProcessingTime() {
        return lastRunAvgClientProcessingTime;
    }

    @Override
    public void run() {
        AtomicInteger clientsConnected = new AtomicInteger(0);
        AtomicBoolean isThereDisconnectedClient = new AtomicBoolean(false);

        List<ClientConnection> clientConnections = new ArrayList<>();
        ExecutorService taskExecutor = Executors.newFixedThreadPool(threadPoolSize);

        try (final AsynchronousServerSocketChannel serverChannel = AsynchronousServerSocketChannel.open()) {
            serverChannel.bind(new InetSocketAddress(port));
            serverChannel.accept(null, new CompletionHandler<AsynchronousSocketChannel, Void>() {
                @Override
                public void completed(AsynchronousSocketChannel clientChannel, Void att) {
                    ClientConnection clientConnection = new ClientConnection(clientChannel, clientsConnected, isThereDisconnectedClient);
                    clientsConnected.incrementAndGet();
                    clientConnections.add(clientConnection);

                    if (serverChannel.isOpen()) {
                        serverChannel.accept(null, this);
                    }
                    clientChannel.read(clientConnection.sizeBuffer, clientConnection, new ReadCallback(taskExecutor));
                }

                @Override
                public void failed(Throwable exc, Void att) {
                    if (serverChannel.isOpen()) {
                        serverChannel.accept(null, this);
                    }
                }
            });
            Thread.currentThread().join();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException ignored) {
        }
        taskExecutor.shutdown();
        while (true) {
            try {
                while (!taskExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                }
                break;
            } catch (InterruptedException ignored) {
            }
        }
        prepareResults(clientConnections);
    }

    static class ReadCallback implements CompletionHandler<Integer, ClientConnection> {
        private final ExecutorService taskExecutor;

        ReadCallback(ExecutorService taskExecutor) {
            this.taskExecutor = taskExecutor;
        }

        @Override
        public void completed(Integer result, ClientConnection clientConnection) {
            clientConnection.curRequestStartTimeQueue.addFirst(System.currentTimeMillis());
            if (result >= 0) {
                if (clientConnection.sizeBuffer.position() >= 4) {
                    clientConnection.sizeBuffer.flip();
                    final int messageSize = clientConnection.sizeBuffer.getInt();
                    clientConnection.messageBuffer = ByteBuffer.allocate(messageSize);
                    clientConnection.clientSocketChannel.read(clientConnection.messageBuffer, clientConnection, new CompletionHandler<>() {
                        @Override
                        public void completed(Integer result, ClientConnection clientConnection) {
                            if (result >= 0) {
                                if (clientConnection.messageBuffer.position() >= messageSize) {
                                    clientConnection.messageBuffer.flip();
                                    taskExecutor.execute(clientConnection.createSortingTask());
                                    clientConnection.messageBuffer = null;
                                    clientConnection.sizeBuffer.clear();
                                    clientConnection.clientSocketChannel.read(clientConnection.sizeBuffer, clientConnection, ReadCallback.this);
                                } else {
                                    clientConnection.clientSocketChannel.read(clientConnection.messageBuffer, clientConnection, this);
                                }
                            } else {
                                try {
                                    System.err.printf("Can't read message. Read %d bytes\n", result);
                                    clientConnection.clientSocketChannel.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failed(Throwable exc, ClientConnection attachment) {
                            try {
                                System.err.println("Message reading failed");
                                exc.printStackTrace();
                                clientConnection.clientSocketChannel.close();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                } else {
                    clientConnection.clientSocketChannel.read(clientConnection.sizeBuffer, clientConnection, this);
                }
            }
        }

        @Override
        public void failed(Throwable exc, ClientConnection clientConnection) {
            try {
                System.err.println("Message size reading failed");
                exc.printStackTrace();
                clientConnection.clientSocketChannel.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void prepareResults(List<ClientConnection> clientConnections) {
        lastRunAvgTaskExecutionTime = clientConnections.stream().flatMap(connection -> connection.taskExecutionIntervals.stream()).filter(executionInterval -> executionInterval.clientsConnected() == clientsNumber && !executionInterval.isThereDisconnectedClient()).map(executionInterval -> executionInterval.end() - executionInterval.start()).mapToLong(x -> x).average().orElseThrow();
        lastRunAvgClientProcessingTime = clientConnections.stream().flatMap(connection -> connection.clientProcessingIntervals.stream()).filter(executionInterval -> executionInterval.clientsConnected() == clientsNumber && !executionInterval.isThereDisconnectedClient()).map(executionInterval -> executionInterval.end() - executionInterval.start()).mapToLong(x -> x).average().orElseThrow();
    }
}
