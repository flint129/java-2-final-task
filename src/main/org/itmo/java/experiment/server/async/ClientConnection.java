package org.itmo.java.experiment.server.async;

import com.google.protobuf.InvalidProtocolBufferException;
import org.itmo.java.experiment.server.Server;
import org.itmo.java.experiment.server.ServerProto.IntegerArray;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ClientConnection {

    final ByteBuffer sizeBuffer;
    ByteBuffer messageBuffer;
    private final AtomicInteger clientsConnected;
    private final AtomicBoolean isThereDisconnectedClient;
    // В асинхронной архитектуре, при маленьком интервале между запросами, часто бывало,
    // что логика при responseBuffer.hasRemaining() == false отрабатывает, когда клиент уже прислал
    // новый запрос из-за чего вощникает много неприятных ситуаций, поэтому пришлось добавить ConcurrentLinkedDeque
    ConcurrentLinkedDeque<Long> curRequestStartTimeQueue = new ConcurrentLinkedDeque<>();
    public final ArrayList<Server.ExecutionInterval> taskExecutionIntervals = new ArrayList<>();
    public final ArrayList<Server.ExecutionInterval> clientProcessingIntervals = new ArrayList<>();
    public final AsynchronousSocketChannel clientSocketChannel;

    public ClientConnection(AsynchronousSocketChannel clientSocketChannel, AtomicInteger clientsConnected, AtomicBoolean isThereDisconnectedClient) {
        this.clientSocketChannel = clientSocketChannel;
        sizeBuffer = ByteBuffer.allocate(Integer.BYTES);
        this.clientsConnected = clientsConnected;
        this.isThereDisconnectedClient = isThereDisconnectedClient;
    }

    void sendToClient(List<Integer> data) {
        IntegerArray integerArray = IntegerArray.newBuilder().setNumberOfElements(data.size()).addAllElements(data).build();
        byte[] byteArray = integerArray.toByteArray();
        ByteBuffer responseBuffer = ByteBuffer.allocate(Integer.BYTES + byteArray.length);
        responseBuffer.putInt(byteArray.length);
        responseBuffer.put(byteArray);
        responseBuffer.flip();

        clientSocketChannel.write(responseBuffer, responseBuffer, new CompletionHandler<>() {
            @Override
            public void completed(Integer result, ByteBuffer responseBuffer) {
                if (responseBuffer.hasRemaining()) {
                    clientSocketChannel.write(responseBuffer, responseBuffer, this);
                } else {
                    long endTime = System.currentTimeMillis();
                    clientProcessingIntervals.add(new Server.ExecutionInterval(curRequestStartTimeQueue.pollLast(), endTime, clientsConnected.get(), isThereDisconnectedClient.get()));
                }
            }

            @Override
            public void failed(Throwable exc, ByteBuffer buffer) {
                System.err.println("Failed to write data to client:");
                exc.printStackTrace();
                try {
                    clientSocketChannel.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    SortArrayTask createSortingTask() {
        try {
            IntegerArray integerArray = IntegerArray.parseFrom(messageBuffer);
            ArrayList<Integer> data = new ArrayList<>(integerArray.getElementsList());
            return new SortArrayTask(data);
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
    }

    public class SortArrayTask implements Runnable {
        private final ArrayList<Integer> data;

        public SortArrayTask(ArrayList<Integer> data) {
            this.data = data;
        }

        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < data.size() - 1; i++) {
                int minIndex = i;
                for (int j = i + 1; j < data.size(); j++) {
                    if (data.get(j) < data.get(minIndex)) {
                        minIndex = j;
                    }
                }
                if (minIndex != i) {
                    int temp = data.get(i);
                    data.set(i, data.get(minIndex));
                    data.set(minIndex, temp);
                }
            }
            long endTime = System.currentTimeMillis();
            taskExecutionIntervals.add(new Server.ExecutionInterval(startTime, endTime, clientsConnected.get(), isThereDisconnectedClient.get()));
            // Костыль, чтобы избежать WritePendingException. К сожалению не
            // придумал как это сделать не влияя на время выполнения запроса
            if (curRequestStartTimeQueue.size() > 1) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            sendToClient(data);
        }
    }
}
