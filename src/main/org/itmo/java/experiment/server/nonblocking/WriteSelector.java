package org.itmo.java.experiment.server.nonblocking;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class WriteSelector implements Runnable {
    public final ConcurrentLinkedQueue<ClientConnection> writeQueue;
    private final AtomicBoolean isThereDisconnectedClient;
    private Selector selector;

    public WriteSelector(ConcurrentLinkedQueue<ClientConnection> writeQueue, AtomicBoolean isThereDisconnectedClient) {
        this.writeQueue = writeQueue;
        this.isThereDisconnectedClient = isThereDisconnectedClient;
    }

    @Override
    public void run() {
        try {
            selector = Selector.open();
            while (!Thread.interrupted()) {
                selector.select();
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey key = keyIterator.next();
                    keyIterator.remove();
                    if (!key.isValid()) {
                        isThereDisconnectedClient.set(true);
                    }
                    ClientConnection clientConnection = (ClientConnection) key.attachment();
                    try {
                        boolean isFinished = clientConnection.write();
                        if (isFinished) {
                            key.cancel();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        key.cancel();
                        clientConnection.clientSocketChannel.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.err.println("Answer write failed. Cancel client");
                        key.cancel();
                    }
                }
                while (!writeQueue.isEmpty()) {
                    ClientConnection clientConnection = writeQueue.poll();
                    try {
                        if (clientConnection.clientSocketChannel.isOpen()) {
                            SelectionKey key = clientConnection.clientSocketChannel.keyFor(selector);
                            if (key == null || !key.isValid()) {
                                clientConnection.clientSocketChannel.register(selector, SelectionKey.OP_WRITE, clientConnection);
                            }
                        }
                    } catch (IOException e) {
                        clientConnection.clientSocketChannel.close();
                    }
                }
            }
            selector.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void wakeup() {
        if (selector != null) {
            selector.wakeup();
        }
    }
}
