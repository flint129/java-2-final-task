package org.itmo.java.experiment.server.nonblocking;

import org.itmo.java.experiment.server.Server;
import org.itmo.java.experiment.server.ServerProto.IntegerArray;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class ClientConnection {

    private final ByteBuffer sizeBuffer;
    private final WriteSelector writeSelector;
    private ByteBuffer messageBuffer;
    // С учетом того, как клиент шлет запросы, нужды в очереди для ответов нет, но решил ее добавить
    // для соответствия той реализации, которая обсуждалась на занятии
    private final ConcurrentLinkedQueue<ByteBuffer> answersBuffersQueue = new ConcurrentLinkedQueue<>();
    private final AtomicInteger clientsConnected;
    private final AtomicBoolean isThereDisconnectedClient;
    private final ExecutorService taskExecutor;
    private final ConcurrentLinkedDeque<Long> curRequestStartTimeQueue = new ConcurrentLinkedDeque<>();
    public final ArrayList<Server.ExecutionInterval> taskExecutionIntervals = new ArrayList<>();
    public final ArrayList<Server.ExecutionInterval> clientProcessingIntervals = new ArrayList<>();
    public final SocketChannel clientSocketChannel;
    private int bytesRead = 0;
    private int messageSize = 0;

    public ClientConnection(SocketChannel clientSocketChannel, ExecutorService taskExecutor, AtomicInteger clientsConnected, AtomicBoolean isThereDisconnectedClient, WriteSelector writeSelector) {
        this.clientSocketChannel = clientSocketChannel;
        this.taskExecutor = taskExecutor;
        sizeBuffer = ByteBuffer.allocate(Integer.BYTES);
        this.clientsConnected = clientsConnected;
        this.isThereDisconnectedClient = isThereDisconnectedClient;
        this.writeSelector = writeSelector;
    }

    boolean read() throws IOException {
        if (bytesRead == 0) {
            curRequestStartTimeQueue.addFirst(System.currentTimeMillis());
        }
        if (bytesRead < Integer.BYTES) {
            int result = clientSocketChannel.read(sizeBuffer);
            if (result == -1) {
                isThereDisconnectedClient.set(true);
                return true;
            }
            bytesRead += result;
            if (bytesRead >= Integer.BYTES) {
                sizeBuffer.flip();
                messageSize = sizeBuffer.getInt();
                messageBuffer = ByteBuffer.allocate(messageSize);
            }
        } else {
            int result = clientSocketChannel.read(messageBuffer);
            if (result == -1) {
                isThereDisconnectedClient.set(true);
                return true;
            }
            bytesRead += result;
            if (bytesRead >= Integer.BYTES + messageSize) {
                messageBuffer.flip();
                IntegerArray integerArray = IntegerArray.parseFrom(messageBuffer);
                sizeBuffer.clear();
                messageBuffer = null;
                bytesRead = 0;
                ArrayList<Integer> data = new ArrayList<>(integerArray.getElementsList());
                taskExecutor.execute(new SortArrayTask(data));
            }
        }
        return false;
    }

    boolean write() throws IOException {
        if (answersBuffersQueue.isEmpty()) {
            return true;
        }
        ByteBuffer answerBuffer = answersBuffersQueue.peek();
        clientSocketChannel.write(answerBuffer);
        if (!answerBuffer.hasRemaining()) {
            long endTime = System.currentTimeMillis();
            clientProcessingIntervals.add(new Server.ExecutionInterval(curRequestStartTimeQueue.pollLast(), endTime, clientsConnected.get(), isThereDisconnectedClient.get()));
            answersBuffersQueue.poll();
        }
        return answersBuffersQueue.isEmpty();
    }

    void addBufferForWrite(List<Integer> data) {
        IntegerArray integerArray = IntegerArray.newBuilder().setNumberOfElements(data.size()).addAllElements(data).build();
        byte[] byteArray = integerArray.toByteArray();
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES + byteArray.length);
        buffer.putInt(byteArray.length);
        buffer.put(byteArray);
        buffer.flip();
        answersBuffersQueue.add(buffer);
        writeSelector.writeQueue.add(ClientConnection.this);
        writeSelector.wakeup();
    }

    private class SortArrayTask implements Runnable {
        private final ArrayList<Integer> data;

        public SortArrayTask(ArrayList<Integer> data) {
            this.data = data;
        }

        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < data.size() - 1; i++) {
                int minIndex = i;
                for (int j = i + 1; j < data.size(); j++) {
                    if (data.get(j) < data.get(minIndex)) {
                        minIndex = j;
                    }
                }
                if (minIndex != i) {
                    int temp = data.get(i);
                    data.set(i, data.get(minIndex));
                    data.set(minIndex, temp);
                }
            }
            long endTime = System.currentTimeMillis();
            taskExecutionIntervals.add(new Server.ExecutionInterval(startTime, endTime, clientsConnected.get(), isThereDisconnectedClient.get()));
            addBufferForWrite(data);
        }
    }
}
