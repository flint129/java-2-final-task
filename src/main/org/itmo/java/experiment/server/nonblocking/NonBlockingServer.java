package org.itmo.java.experiment.server.nonblocking;

import org.itmo.java.experiment.server.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class NonBlockingServer implements Server {
    private final int threadPoolSize;
    private final int port;
    private final int clientsNumber;
    private double lastRunAvgTaskExecutionTime;
    private double lastRunAvgClientProcessingTime;

    public NonBlockingServer(int port, int clientsNumber,int threadPoolSize) {
        this.port = port;
        this.clientsNumber = clientsNumber;
        this.threadPoolSize = threadPoolSize;
    }

    @Override
    public Double getLastRunAvgTaskExecutionTime() {
        return lastRunAvgTaskExecutionTime;
    }

    @Override
    public Double getLastRunAvgClientProcessingTime() {
        return lastRunAvgClientProcessingTime;
    }

    @Override
    public void run() {
        AtomicInteger clientsConnected = new AtomicInteger(0);
        AtomicBoolean isThereDisconnectedClient = new AtomicBoolean(false);

        ConcurrentLinkedQueue<ClientConnection> connectionQueue = new ConcurrentLinkedQueue<>();
        ConcurrentLinkedQueue<ClientConnection> answersQueue = new ConcurrentLinkedQueue<>();

        List<ClientConnection> clientConnections = new ArrayList<>();
        ExecutorService taskExecutor = Executors.newFixedThreadPool(threadPoolSize);
        ReadSelector readSelector = new ReadSelector(connectionQueue, isThereDisconnectedClient);
        WriteSelector writeSelector = new WriteSelector(answersQueue, isThereDisconnectedClient);

        Thread readSelectorThread = new Thread(readSelector);
        Thread writeSelectorThread = new Thread(writeSelector);
        readSelectorThread.start();
        writeSelectorThread.start();

        acceptClientsLoop(clientsConnected, taskExecutor, isThereDisconnectedClient, writeSelector, clientConnections, connectionQueue, readSelector);

        taskExecutor.shutdown();
        readSelectorThread.interrupt();
        readSelector.wakeup();
        writeSelectorThread.interrupt();
        writeSelector.wakeup();
        try {
            readSelectorThread.join();
            writeSelectorThread.join();
            while (true) {
                try {
                    while (!taskExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                    }
                    break;
                } catch (InterruptedException ignored) {
                }
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        prepareResults(clientConnections);
    }

    private void acceptClientsLoop(AtomicInteger clientsConnected, ExecutorService taskExecutor, AtomicBoolean isThereDisconnectedClient, WriteSelector writeSelector, List<ClientConnection> clientConnections, ConcurrentLinkedQueue<ClientConnection> connectionQueue, ReadSelector readSelector) {
        try (ServerSocketChannel serverSocketChannel = ServerSocketChannel.open()) {
            serverSocketChannel.bind(new InetSocketAddress(port));
            while (!Thread.currentThread().isInterrupted()) {
                SocketChannel clientSocketChannel = serverSocketChannel.accept();
                clientSocketChannel.configureBlocking(false);
                clientsConnected.incrementAndGet();
                ClientConnection clientConnection = new ClientConnection(clientSocketChannel, taskExecutor, clientsConnected, isThereDisconnectedClient, writeSelector);
                clientConnections.add(clientConnection);
                connectionQueue.add(clientConnection);
                readSelector.wakeup();
            }
        } catch (ClosedByInterruptException ignored) {
            Thread.interrupted();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void prepareResults(List<ClientConnection> clientConnections) {
        lastRunAvgTaskExecutionTime = clientConnections.stream().flatMap(connection -> connection.taskExecutionIntervals.stream()).filter(executionInterval -> executionInterval.clientsConnected() == clientsNumber && !executionInterval.isThereDisconnectedClient()).map(executionInterval -> executionInterval.end() - executionInterval.start()).mapToLong(x -> x).average().orElseThrow();
        lastRunAvgClientProcessingTime = clientConnections.stream().flatMap(connection -> connection.clientProcessingIntervals.stream()).filter(executionInterval -> executionInterval.clientsConnected() == clientsNumber && !executionInterval.isThereDisconnectedClient()).map(executionInterval -> executionInterval.end() - executionInterval.start()).mapToLong(x -> x).average().orElseThrow();
    }
}
