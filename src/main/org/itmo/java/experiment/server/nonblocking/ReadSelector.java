package org.itmo.java.experiment.server.nonblocking;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class ReadSelector implements Runnable {
    private final ConcurrentLinkedQueue<ClientConnection> connectionQueue;
    private final AtomicBoolean isThereDisconnectedClient;
    private Selector selector;

    public ReadSelector(ConcurrentLinkedQueue<ClientConnection> connectionQueue, AtomicBoolean isThereDisconnectedClient) {
        this.connectionQueue = connectionQueue;
        this.isThereDisconnectedClient = isThereDisconnectedClient;
    }

    @Override
    public void run() {
        try {
            selector = Selector.open();
            while (!Thread.interrupted()) {
                selector.select();
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey key = keyIterator.next();
                    if (!key.isValid()) {
                        isThereDisconnectedClient.set(true);
                        keyIterator.remove();
                    }
                    ClientConnection clientConnection = (ClientConnection) key.attachment();
                    try {
                        boolean isFinished = clientConnection.read();
                        if (isFinished) {
                            key.cancel();
                            clientConnection.clientSocketChannel.close();
                        }
                    } catch (IOException e) {
                        key.cancel();
                        clientConnection.clientSocketChannel.close();
                    }
                    keyIterator.remove();
                }
                while (!connectionQueue.isEmpty()) {
                    ClientConnection clientConnection = connectionQueue.poll();
                    try {
                        clientConnection.clientSocketChannel.register(selector, SelectionKey.OP_READ, clientConnection);
                    } catch (IOException e) {
                        clientConnection.clientSocketChannel.close();
                    }
                }
            }
            selector.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void wakeup() {
        if (selector != null) {
            selector.wakeup();
        }
    }
}
