package org.itmo.java.experiment.server;

public interface Server extends Runnable {
    Double getLastRunAvgTaskExecutionTime();

    Double getLastRunAvgClientProcessingTime();

    record ExecutionInterval(long start, long end, int clientsConnected, boolean isThereDisconnectedClient) {
    }
}
