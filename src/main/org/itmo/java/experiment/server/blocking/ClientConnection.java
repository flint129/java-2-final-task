package org.itmo.java.experiment.server.blocking;

import org.itmo.java.experiment.server.Server;
import org.itmo.java.experiment.server.ServerProto.IntegerArray;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class ClientConnection {
    private final Thread readThread;
    private final ExecutorService writeExecutor;
    private final SocketChannel clientSocketChannel;
    private final ExecutorService taskExecutor;
    private final AtomicInteger clientsConnected;
    private final AtomicBoolean isThereDisconnectedClient;
    private long curRequestStartTime;
    public final ArrayList<Server.ExecutionInterval> taskExecutionIntervals = new ArrayList<>();
    public final ArrayList<Server.ExecutionInterval> clientProcessingIntervals = new ArrayList<>();


    public ClientConnection(SocketChannel clientSocketChannel, ExecutorService taskExecutor, AtomicInteger clientsConnected, AtomicBoolean isThereDisconnectedClient) {
        this.clientSocketChannel = clientSocketChannel;
        this.taskExecutor = taskExecutor;
        readThread = new Thread(new Read());
        writeExecutor = Executors.newSingleThreadExecutor();
        this.clientsConnected = clientsConnected;
        this.isThereDisconnectedClient = isThereDisconnectedClient;

        readThread.start();
    }

    void waitConnectionFinish() {
        readThread.interrupt();
        writeExecutor.shutdown();
        while (true) {
            try {
                readThread.join();
                while (!writeExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                }
                return;
            } catch (InterruptedException ignored) {
            }
        }
    }

    private class Read implements Runnable {

        @Override
        public void run() {
            try {
                while (clientSocketChannel.isOpen()) {
                    ByteBuffer sizeBuffer = ByteBuffer.allocate(Integer.BYTES);
                    int result = read(sizeBuffer, Integer.BYTES);
                    curRequestStartTime = System.currentTimeMillis();
                    if (result == -1) {
                        break;
                    }
                    int messageSize = sizeBuffer.getInt();

                    ByteBuffer messageBuffer = ByteBuffer.allocate(messageSize);
                    result = read(messageBuffer, messageSize);
                    if (result == -1) {
                        break;
                    }

                    IntegerArray integerArray = IntegerArray.parseFrom(messageBuffer);
                    ArrayList<Integer> data = new ArrayList<>(integerArray.getElementsList());
                    taskExecutor.execute(new SortArrayTask(data));
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                isThereDisconnectedClient.set(true);
            }
        }

        int read(ByteBuffer buffer, int n) throws IOException {
            buffer.clear();
            int bytesRead = 0;
            while (bytesRead < n) {
                int result = clientSocketChannel.read(buffer);
                if (result == -1) {
                    return -1;
                }
                bytesRead += result;
            }
            buffer.flip();
            return bytesRead;
        }
    }

    private class SortArrayTask implements Runnable {
        private final ArrayList<Integer> data;

        public SortArrayTask(ArrayList<Integer> data) {
            this.data = data;
        }

        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            for (int i = 0; i < data.size() - 1; i++) {
                int minIndex = i;
                for (int j = i + 1; j < data.size(); j++) {
                    if (data.get(j) < data.get(minIndex)) {
                        minIndex = j;
                    }
                }
                if (minIndex != i) {
                    int temp = data.get(i);
                    data.set(i, data.get(minIndex));
                    data.set(minIndex, temp);
                }
            }
            long endTime = System.currentTimeMillis();
            taskExecutionIntervals.add(new Server.ExecutionInterval(startTime, endTime, clientsConnected.get(), isThereDisconnectedClient.get()));
            writeExecutor.execute(new WriteResult(data));
        }
    }

    class WriteResult implements Runnable {
        private final ArrayList<Integer> data;

        public WriteResult(ArrayList<Integer> data) {
            this.data = data;
        }

        @Override
        public void run() {
            try {
                IntegerArray integerArray = IntegerArray.newBuilder().setNumberOfElements(data.size()).addAllElements(data).build();
                byte[] byteArray = integerArray.toByteArray();
                ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES + byteArray.length);
                buffer.putInt(byteArray.length);
                buffer.put(byteArray);
                buffer.flip();
                while (buffer.hasRemaining()) {
                    clientSocketChannel.write(buffer);
                }
                long endTime = System.currentTimeMillis();
                clientProcessingIntervals.add(new Server.ExecutionInterval(curRequestStartTime, endTime, clientsConnected.get(), isThereDisconnectedClient.get()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
