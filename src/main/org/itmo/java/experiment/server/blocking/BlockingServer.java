package org.itmo.java.experiment.server.blocking;

import org.itmo.java.experiment.server.Server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class BlockingServer implements Server {
    private final int threadPoolSize;
    private final int port;
    private final int clientsNumber;
    private double lastRunAvgTaskExecutionTime;
    private double lastRunAvgClientProcessingTime;


    public BlockingServer(int port, int clientsNumber, int threadPoolSize) {
        this.port = port;
        this.clientsNumber = clientsNumber;
        this.threadPoolSize = threadPoolSize;
    }

    @Override
    public Double getLastRunAvgTaskExecutionTime() {
        return lastRunAvgTaskExecutionTime;
    }

    @Override
    public Double getLastRunAvgClientProcessingTime() {
        return lastRunAvgClientProcessingTime;
    }

    @Override
    public void run() {
        AtomicInteger clientsConnected = new AtomicInteger(0);
        AtomicBoolean isThereDisconnectedClient = new AtomicBoolean(false);

        List<ClientConnection> clientConnections = new ArrayList<>();
        ExecutorService taskExecutor = Executors.newFixedThreadPool(threadPoolSize);

        try (ServerSocketChannel serverSocketChannel = ServerSocketChannel.open()) {
            serverSocketChannel.bind(new InetSocketAddress(port));
            while (!Thread.interrupted()) {
                SocketChannel clientSocketChannel = serverSocketChannel.accept();
                clientsConnected.incrementAndGet();
                clientConnections.add(new ClientConnection(clientSocketChannel, taskExecutor, clientsConnected, isThereDisconnectedClient));
            }
        } catch (ClosedByInterruptException ignored) {
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        taskExecutor.shutdown();
        clientConnections.forEach(ClientConnection::waitConnectionFinish);
        while (true) {
            try {
                while (!taskExecutor.awaitTermination(1, TimeUnit.SECONDS)) {
                }
                break;
            } catch (InterruptedException ignored) {
            }
        }
        prepareResults(clientConnections);
    }

    private void prepareResults(List<ClientConnection> clientConnections) {
        lastRunAvgTaskExecutionTime = clientConnections.stream().flatMap(connection -> connection.taskExecutionIntervals.stream()).filter(executionInterval -> executionInterval.clientsConnected() == clientsNumber && !executionInterval.isThereDisconnectedClient()).map(executionInterval -> executionInterval.end() - executionInterval.start()).mapToLong(x -> x).average().orElseThrow();
        lastRunAvgClientProcessingTime = clientConnections.stream().flatMap(connection -> connection.clientProcessingIntervals.stream()).filter(executionInterval -> executionInterval.clientsConnected() == clientsNumber && !executionInterval.isThereDisconnectedClient()).map(executionInterval -> executionInterval.end() - executionInterval.start()).mapToLong(x -> x).average().orElseThrow();
    }
}
