package org.itmo.java.experiment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.cli.*;
import org.itmo.java.experiment.client.Client;
import org.itmo.java.experiment.server.async.AsyncServer;
import org.itmo.java.experiment.server.blocking.BlockingServer;
import org.itmo.java.experiment.server.Server;
import org.itmo.java.experiment.server.nonblocking.NonBlockingServer;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExperimentLauncher {
    final public static int THREAD_POOL_SIZE = 4;
    final public static int SERVER_PORT = 8081;
    final public static String SERVER_HOST = "localhost";
    final public static int WAITING_SERVER_START_DELAY_MILLIS = 1000;
    final static int WARMUP_ITERATIONS = 10;
    final static int WARMUP_REQUESTS_NUMBER = 10;
    final static int WARMUP_TASK_SIZE = 1000;
    final static int WARMUP_CLIENTS_NUMBER = 3;
    final static int WARMUP_REQUESTS_INTERVAL_MILLIS = 10;

    record ExperimentResult(double avgTaskExecutionTimeOnServer, double avgClientProcessingTimeOnServer,
                            double avgRequestProcessingTimeOnClient) {
    }

    public static void main(String[] args) {
        Options options = getOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLine cmd = parser.parse(options, args);
            String serverArch = cmd.getOptionValue("server-architecture");
            Integer requestsNumber = Integer.parseInt(cmd.getOptionValue("requests"));
            IntExperimentParameter taskSize = new IntExperimentParameter(cmd.getOptionValue("task-size"), "Task size");
            IntExperimentParameter clientsNumber = new IntExperimentParameter(cmd.getOptionValue("clients-number"), "Clients number");
            IntExperimentParameter requestIntervalMillis = new IntExperimentParameter(cmd.getOptionValue("requests-interval"), "Requests interval");
            if ((taskSize.isRange ? 1 : 0) + (clientsNumber.isRange ? 1 : 0) + (requestIntervalMillis.isRange ? 1 : 0) != 1) {
                System.err.println("Incorrect experiment configuration");
                System.exit(1);
            }

            warmup(serverArch);

            List<ExperimentResult> experimentResults = new ArrayList<>();
            for (int curTaskSize : taskSize) {
                for (int curClientsNumber : clientsNumber) {
                    for (int curRequestIntervalMillis : requestIntervalMillis) {
                        System.out.printf("task size: %d, clients number: %d, request interval millis: %d\n", curTaskSize, curClientsNumber, curRequestIntervalMillis);
                        experimentResults.add(runExperiment(serverArch, requestsNumber, curTaskSize, curClientsNumber, curRequestIntervalMillis));
                    }
                }
            }
            saveResults(args, experimentResults, serverArch, requestsNumber, taskSize, clientsNumber, requestIntervalMillis);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);
            System.exit(1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void warmup(String serverArch) throws ParseException {
        for (int i = 0; i < WARMUP_ITERATIONS; i++) {
            runExperiment(serverArch, WARMUP_REQUESTS_NUMBER, WARMUP_TASK_SIZE, WARMUP_CLIENTS_NUMBER, WARMUP_REQUESTS_INTERVAL_MILLIS);
        }
    }

    private static Options getOptions() {
        Options options = new Options();

        Option serverArchOption = new Option("s", "server-architecture", true, "server architecture");
        serverArchOption.setRequired(true);
        options.addOption(serverArchOption);

        Option requestsNumberOption = new Option("x", "requests", true, "number of requests from client");
        requestsNumberOption.setRequired(true);
        options.addOption(requestsNumberOption);

        Option taskSizeOption = new Option("n", "task-size", true, "server task size");
        taskSizeOption.setRequired(true);
        options.addOption(taskSizeOption);

        Option clientsNumberOption = new Option("m", "clients-number", true, "clients number");
        clientsNumberOption.setRequired(true);
        options.addOption(clientsNumberOption);

        Option requestsIntervalOption = new Option("d", "requests-interval", true, "interval between requests from one client in milliseconds");
        requestsIntervalOption.setRequired(true);
        options.addOption(requestsIntervalOption);
        return options;
    }

    private static Server createServer(String serverArch, int clientsNumber) throws ParseException {
        return switch (serverArch) {
            case "blocking" -> new BlockingServer(SERVER_PORT, clientsNumber, THREAD_POOL_SIZE);
            case "nonblocking" -> new NonBlockingServer(SERVER_PORT, clientsNumber, THREAD_POOL_SIZE);
            case "async" -> new AsyncServer(SERVER_PORT, clientsNumber, THREAD_POOL_SIZE);
            default -> throw new ParseException("Unknown server architecture");
        };
    }

    private static ExperimentResult runExperiment(String serverArch, Integer requestsNumber, Integer taskSize, Integer clientsNumber, Integer requestIntervalMillis) throws ParseException {
        Server server = createServer(serverArch, clientsNumber);
        Thread serverThread = new Thread(server);
        serverThread.start();
        try {
            Thread.sleep(WAITING_SERVER_START_DELAY_MILLIS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        List<Client> clients = new ArrayList<>();
        List<Thread> clientThreads = new ArrayList<>();
        for (int i = 0; i < clientsNumber; i++) {
            Client client = new Client(SERVER_HOST, SERVER_PORT, requestsNumber, taskSize, requestIntervalMillis);
            clients.add(client);
            clientThreads.add(new Thread(client));
        }
        clientThreads.forEach(Thread::start);

        try {
            for (Thread clientThread : clientThreads) {
                clientThread.join();
            }
            serverThread.interrupt();
            serverThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        double avgRequestProcessingTimeOnClient = clients.stream().mapToDouble(Client::getAvgRequestProcessingTime).average().orElseThrow();
        return new ExperimentResult(server.getLastRunAvgTaskExecutionTime(), server.getLastRunAvgClientProcessingTime(), avgRequestProcessingTimeOnClient);
    }

    private static void saveResults(String[] args, List<ExperimentResult> results, String serverArch, Integer requestsNumber, IntExperimentParameter taskSize, IntExperimentParameter clientsNumber, IntExperimentParameter requestIntervalMillis) throws IOException {
        LocalDateTime dateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = dateTime.format(formatter);
        Path experimentDir = createExperimentDirectory(serverArch);
        Map<String, Object> params = Map.of("cli_args", String.join(" ", args), "server_arch", serverArch, "requests_number", requestsNumber, "task_size", taskSize, "clients_number", clientsNumber, "request_interval_millis", requestIntervalMillis, "experiment_date_time", formattedDateTime);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Path experimentParamsPath = experimentDir.resolve("experiment.json");
        try (FileWriter writer = new FileWriter(experimentParamsPath.toFile())) {
            gson.toJson(params, writer);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<Double> taskTimes = results.stream().map(ExperimentResult::avgTaskExecutionTimeOnServer).collect(Collectors.toList());
        List<Double> clientTimes = results.stream().map(ExperimentResult::avgClientProcessingTimeOnServer).collect(Collectors.toList());
        List<Double> requestTimes = results.stream().map(ExperimentResult::avgRequestProcessingTimeOnClient).collect(Collectors.toList());
        try {
            saveJson(experimentDir.resolve("avg_task_execution_time_on_server.json"), taskTimes);
            saveJson(experimentDir.resolve("avg_client_processing_time_on_server.json"), clientTimes);
            saveJson(experimentDir.resolve("avg_request_processing_time_on_client.json"), requestTimes);

//            PlotCreator plotCreator = new PlotCreator(experimentDir, serverArch, requestsNumber, taskSize, clientsNumber, requestIntervalMillis);
//            plotCreator.createPlots();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Path createExperimentDirectory(String serverArch) throws IOException {
        Path resultsDir = Paths.get(".", "results", serverArch);
        if (!Files.exists(resultsDir)) {
            try {
                Files.createDirectories(resultsDir);
            } catch (IOException e) {
                throw new RuntimeException("Could not create results directory", e);
            }
        }

        int maxDir = 0;
        try (DirectoryStream<Path> dirs = Files.newDirectoryStream(resultsDir)) {
            for (Path path : dirs) {
                String name = path.getFileName().toString();
                try {
                    maxDir = Math.max(maxDir, Integer.parseInt(name));
                } catch (NumberFormatException ignored) {
                }
            }
        }

        int newDir = maxDir + 1;
        Path experimentDir = resultsDir.resolve(String.valueOf(newDir));
        try {
            Files.createDirectory(experimentDir);
        } catch (IOException e) {
            throw new RuntimeException("Could not create experiment directory", e);
        }
        return experimentDir;
    }

    private static void saveJson(Path path, List<Double> data) throws IOException {
        Map<String, Object> map = new HashMap<>();
        map.put("data", data);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileWriter writer = new FileWriter(path.toFile())) {
            gson.toJson(map, writer);
        }
    }
}