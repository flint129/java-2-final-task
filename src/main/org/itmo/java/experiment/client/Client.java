package org.itmo.java.experiment.client;


import org.itmo.java.experiment.server.ServerProto.IntegerArray;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Random;

public class Client implements Runnable {
    final static int CONNECT_RETRY_TIMEOUT_MILLIS = 1000;
    private double avgRequestProcessingTime;
    private final String serverHost;
    private final Integer serverPort;
    private final Integer requestsNumber;
    private final Integer taskSize;
    private final Integer requestIntervalMillis;

    public Client(String serverHost, Integer serverPort, Integer requestsNumber, Integer taskSize, Integer requestIntervalMillis) {
        this.serverHost = serverHost;
        this.serverPort = serverPort;
        this.requestsNumber = requestsNumber;
        this.taskSize = taskSize;
        this.requestIntervalMillis = requestIntervalMillis;
    }

    public Double getAvgRequestProcessingTime() {
        return avgRequestProcessingTime;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();
        List<Integer> numbers = new Random().ints(taskSize, -100, 100).boxed().toList();
        List<Integer> referenceSortedNumbers = numbers.stream().sorted().toList();
        try {
            try (SocketChannel socketChannel = SocketChannel.open()) {
                while (true) {
                    try {
                        socketChannel.connect(new InetSocketAddress(serverHost, serverPort));
                        break;
                    } catch (ConnectException e) {
                        Thread.sleep(CONNECT_RETRY_TIMEOUT_MILLIS);
                    }
                }
                IntegerArray integerArray = IntegerArray.newBuilder().setNumberOfElements(numbers.size()).addAllElements(numbers).build();
                byte[] byteArray = integerArray.toByteArray();
                ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES + byteArray.length);

                for (int curRequestNumber = 0; curRequestNumber < requestsNumber; curRequestNumber++) {
                    List<Integer> sortedNumbers = makeRequest(buffer, byteArray, socketChannel);

                    // TODO remove
                    if (!referenceSortedNumbers.equals(sortedNumbers)) {
                        throw new RuntimeException("array not sorted");
                    }
//                    System.out.println("Sorted array received: " + sortedNumbers);
                    Thread.sleep(requestIntervalMillis);
                }
            } catch (InterruptedException e) {
                System.err.println("Client interrupted before sending all requests");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        long executionTime = System.currentTimeMillis() - startTime;
        avgRequestProcessingTime = (double) (executionTime - (long) requestIntervalMillis * requestsNumber) / requestsNumber;
    }

    private static List<Integer> makeRequest(ByteBuffer buffer, byte[] byteArray, SocketChannel socketChannel) throws IOException {
        buffer.clear();
        buffer.putInt(byteArray.length);
        buffer.put(byteArray);
        buffer.flip();
        while (buffer.hasRemaining()) {
            socketChannel.write(buffer);
        }

        buffer.clear();
        buffer.limit(Integer.BYTES);
        while (buffer.hasRemaining()) {
            socketChannel.read(buffer);
        }
        buffer.flip();
        int messageSize = buffer.getInt();

        ByteBuffer responseBuffer = ByteBuffer.allocate(messageSize);
        while (responseBuffer.hasRemaining()) {
            socketChannel.read(responseBuffer);
        }
        responseBuffer.flip();

        IntegerArray sortedArray = IntegerArray.parseFrom(responseBuffer.array());
        return sortedArray.getElementsList();
    }
}
