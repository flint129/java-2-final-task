package org.itmo.java.experiment;

import com.google.gson.Gson;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class PlotCreator {
    private final Path experimentDir;
    private final Gson gson;
    private final IntExperimentParameter taskSize;
    private final IntExperimentParameter clientsNumber;
    private final IntExperimentParameter requestIntervalMillis;
    private final String serverArch;
    private final Integer requestsNumber;

    public PlotCreator(Path experimentDir, String serverArch, Integer requestsNumber, IntExperimentParameter taskSize, IntExperimentParameter clientsNumber, IntExperimentParameter requestIntervalMillis) {
        this.experimentDir = experimentDir;
        this.gson = new Gson();
        this.taskSize = taskSize;
        this.clientsNumber = clientsNumber;
        this.requestIntervalMillis = requestIntervalMillis;
        this.serverArch = serverArch;
        this.requestsNumber = requestsNumber;
    }

    public void createPlots() throws IOException {
        IntExperimentParameter rangeParameter;
        String description;
        if (this.requestIntervalMillis.isRange) {
            rangeParameter = this.requestIntervalMillis;
            description = createDescription(clientsNumber, taskSize);
        } else if (this.clientsNumber.isRange) {
            rangeParameter = this.clientsNumber;
            description = createDescription(requestIntervalMillis, taskSize);
        } else if (this.taskSize.isRange) {
            rangeParameter = this.taskSize;
            description = createDescription(requestIntervalMillis, clientsNumber);
        } else {
            throw new RuntimeException("Plot creation failed. Range parameter of experiment not found");
        }
        createPlot("avg_task_execution_time_on_server.json", "Average Task Execution Time On Server", rangeParameter, description);
        createPlot("avg_client_processing_time_on_server.json", "Average Client Processing Time On Server", rangeParameter, description);
        createPlot("avg_request_processing_time_on_client.json", "Average Request Processing Time On Client", rangeParameter, description);
    }

    private String createDescription(IntExperimentParameter parameter1, IntExperimentParameter parameter2) {
        return String.format("server architecture: %s, requests number: %s, %s: %d, %s: %d", serverArch, requestsNumber, parameter1.name, parameter1.startValue, parameter2.name, parameter2.startValue);
    }

    private void createPlot(String metricFileName, String chartTitle, IntExperimentParameter rangeParameter, String description) throws IOException {
        Path metricFilePath = experimentDir.resolve(metricFileName);
        if (!Files.exists(metricFilePath)) {
            System.out.println("Metric file not found: " + metricFilePath);
            return;
        }
        Map<?, ?> metricData = gson.fromJson(new FileReader(metricFilePath.toFile()), Map.class);
        List<Double> data = (List<Double>) metricData.get("data");
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        int i = 0;
        for (Integer paramValue : rangeParameter) {
            dataset.addValue(data.get(i), "Series1", String.valueOf(paramValue));
            i++;
        }

        JFreeChart chart = ChartFactory.createLineChart(chartTitle, rangeParameter.name, "time, ms", dataset, PlotOrientation.VERTICAL, false, true, false);
        chart.addSubtitle(new TextTitle(description));

        Path plotPath = experimentDir.resolve(metricFileName.replace(".json", ".png"));
        ChartUtils.saveChartAsPNG(plotPath.toFile(), chart, 800, 600);
    }
}